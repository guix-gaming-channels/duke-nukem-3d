;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nonfree packages duke-nukem-3d)
  #:use-module ((srfi srfi-1) #:select (second)) ; "zip" conflicts with the "zip" compression package.
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system trivial)
  ;; Inputs
  #:use-module (gnu packages audio)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module ((gnu packages gl) #:select (glu))
  #:use-module ((gnu packages gtk) #:select (gtk+-2))
  #:use-module (gnu packages pkg-config)
  #:use-module ((gnu packages sdl))
  #:use-module ((gnu packages video) #:select (libvpx))
  #:use-module ((gnu packages xiph) #:select (libvorbis flac)))

(define (version-minor version-string)
  "Return minor version number.
For example, (version-minor \"2.1.47.4.23\") returns \"1\"."
  (second (string-split version-string #\.)))

(define duke-nukem-3d-directory "share/dukenukem3d")

(define-public duke-nukem-3d-shareware
  (package
    (name "duke-nukem-3d-shareware")
    (version "1.3d")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "http://hendricks266.duke4.net/files/3dduke13_data.7z"))
       (sha256
        (base32 "1v0d0pr09m3dcnylyckv5731s4rgknp1dagr2mm4mr0n9k5w7k9z"))))
    (build-system trivial-build-system)
    (native-inputs
     `(("p7zip" ,p7zip)))
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (setenv "PATH" (string-append
                         (assoc-ref %build-inputs "p7zip") "/bin" ":"))
         (invoke "7z" "x" (assoc-ref %build-inputs "source"))
         (let ((data (string-append (assoc-ref %outputs "out") "/" ,duke-nukem-3d-directory)))
           (mkdir-p data)
           (copy-file "DUKE.RTS" (string-append data "/duke.rts"))
           (copy-file "DUKE3D.GRP" (string-append data "/duke3d.grp")))
         #t)))
    (synopsis "Data files from Duke Nukem 3D")
    (description "Those are only the data files from Duke Nukem 3D.
You'll need eduke32 to play.")
    (home-page "https://legacy.3drealms.com/duke3d/")
    (license ((@@ (guix licenses) license) "Duke Nukem 3D license"
              ;; TODO: License?
              "https://legacy.3drealms.com/duke3d/"
              ""))))

(define-public duke-nukem-3d-high-resolution-pack
  (package
    (name "duke-nukem-3d-high-resolution-pack")
    (version "5.4")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "http://www.duke4.org/files/nightfright/hrp/duke3d_hrp.zip"))
       (file-name (string-append name "-" version))
       (sha256
        (base32 "19bqvippx8n46x772gi5k7dgl0j4bvik643k8580a4agdd2xy5zj"))))
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (let ((dir (string-append (assoc-ref %outputs "out")
                                   "/" ,duke-nukem-3d-directory)))
           (mkdir-p dir)
           (copy-file (assoc-ref %build-inputs "source")
                      (string-append dir "/duke3d_hrp.zip")))
         #t)))
    (synopsis "High definition textures and models for Duke Nukem 3D")
    (description "For all Duke fans who want to play the game again in a modern
Windows environment with 3D accelerated graphics, the Duke3D community has
created the High Resolution Pack (HRP).  Utilizing the amazing skills of various
texturing and modelling artists, the project´s goal is to replace all textures
and sprites with high-res versions, optimizing it for latest OpenGL ports.")
    (home-page "http://hrp.duke4.net/")
    (license ((@@ (guix licenses) license) "HRP art license"
              "http://hrp.duke4.net/hrp_art_license.txt"
              ""))))

(define-public duke-nukem-3d-high-resolution-pack-duke-dc
  (package
    (name "duke-nukem-3d-high-resolution-pack-duke-dc")
    (version "1.64")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "http://www.duke4.org/files/nightfright/related/"
                           "dukedc_hrp.zip"))
       (file-name (string-append name "-" version))
       (sha256
        (base32 "04ml1w2hgjgskdqg8jbfpcyrjm4c7cwq75wc8b6fjx7bid9mbcrx"))))
    (build-system trivial-build-system)
    (propagated-inputs
     `(("hrp" ,duke-nukem-3d-high-resolution-pack)))
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (let ((dir (string-append (assoc-ref %outputs "out")
                                   "/" ,duke-nukem-3d-directory)))
           (mkdir-p dir)
           (copy-file (assoc-ref %build-inputs "source")
                      (string-append dir "/dukedc_hrp.zip")))
         #t)))
    (synopsis "High definition textures and models for Duke Nukem 3D: Duke It Out In D.C.")
    (description "This file contains data for using the Duke3D High-Resolution
Pack (HRP) with the addon \"Duke It Out In D.C.\".  It contains replacements
that work as some kind of override for some of the HRP textures.")
    (home-page "http://hrp.duke4.net/")
    (license ((@@ (guix licenses) license) "HRP art license"
              "http://hrp.duke4.net/hrp_art_license.txt"
              ""))))

(define-public duke-nukem-3d-xxx-pack
  (package
    (name "duke-nukem-3d-xxx-pack")
    (version "1.33")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "http://www.duke4.org/files/nightfright/related/"
                           "duke3d_xxx.zip"))
       (file-name (string-append name "-" version))
       (sha256
        (base32 "0d5mhjd2n9lqkxa17h8mi77apa7q6b8nklrvz8dml5qa83k165y3"))))
    (build-system trivial-build-system)
    (propagated-inputs
     `(("hrp" ,duke-nukem-3d-high-resolution-pack)))
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (let ((dir (string-append (assoc-ref %outputs "out")
                                   "/" ,duke-nukem-3d-directory)))
           (mkdir-p dir)
           (copy-file (assoc-ref %build-inputs "source")
                      (string-append dir "/duke3d_xxx.zip")))
         #t)))
    (synopsis "Nudity files for Duke Nukem 3D")
    (description "This pack is meant for those who want to play Duke Nukem 3D
with the Highres Pack (HRP) and also like to see... errr... *cough* well, the
babes you encounter wearing less stuff.")
    (home-page "http://hrp.duke4.net/")
    (license ((@@ (guix licenses) license) "HRP art license"
              "http://hrp.duke4.net/hrp_art_license.txt"
              ""))))

(define-public duke-nukem-3d-roland-sc-55-music-pack
  (package
    (name "duke-nukem-3d-roland-sc-55-music-pack")
    (version "4.02")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "http://www.duke4.org/files/nightfright/music/"
                           "duke3d_music-sc55.zip"))
       (file-name (string-append name "-" version))
       (sha256
        (base32 "11wh2fpmz7j3107fkwlkb67nral2p3na90wa6dqli6v2nm7mf8z1"))))
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (let ((dir (string-append (assoc-ref %outputs "out")
                                   "/" ,duke-nukem-3d-directory)))
           (mkdir-p dir)
           (copy-file (assoc-ref %build-inputs "source")
                      (string-append dir "/duke3d_music-sc55.zip")))
         #t)))
    (synopsis "Music modification for Duke Nukem 3D")
    (description "This pack is meant for those who want to play Duke Nukem 3D
with the Highres Pack (HRP) and also like to have high quality music instead of
listening to the old MIDI soundtrack.  It also works for anyone who just wants
to have a music replacement for the game without having any HRP installed.

The music was recorded by MusicallyInspired with a Roland SC-55 synthesizer.")
    (home-page "http://hrp.duke4.net/")
    (license ((@@ (guix licenses) license) "GPLv2?"
              "No URL"
              ""))))

(define-public duke-nukem-3d-duke-it-out-in-dc-music-pack
  (package
    (name "duke-nukem-3d-duke-it-out-in-dc-music-pack")
    (version "2.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "http://www.duke4.org/files/nightfright/music/dukedc_music"
                           (version-major version) (version-minor version)
                           ".zip"))
       (file-name (string-append name "-" version))
       (sha256
        (base32 "1i3hcc4j1m6pwkb919qjam0gq6q6ixnaz911xrcjd1dyyp8jpzyf"))))
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (let ((dir (string-append (assoc-ref %outputs "out")
                                   "/" ,duke-nukem-3d-directory)))
           (mkdir-p dir)
           (copy-file (assoc-ref %build-inputs "source")
                      (string-append dir "/duke3d_music"
                                     ,(version-major version)
                                     ,(version-minor version)
                                     ".zip")))
         #t)))
    (synopsis "Custom soundtrack for Duke Nukem 3D's Duke It Out in D.C.")
    (description "This music pack is optional since it features music which was
not part of the addon at all.  (Standard Duke Nukem 3D music from episode 3 was
used!)")
    (home-page "http://hrp.duke4.net/")
    (license ((@@ (guix licenses) license) "No license"
              "No URL"
              ""))))

;; TODO: Write importer for eduke32 addon compilation?  Maybe not worth it.
(define duke-nukem-3d-eduke32-addon-compilation
  (package
    (name "duke-nukem-3d-eduke32-addon-compilation")
    (version "3.13")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "http://www.duke4.org/files/nightfright/misc/eduke32_addons"
                           (version-major version) (version-minor version) ".exe"))
       (sha256
        (base32 "067gdlm3xwbih4ygvwz5bfq6hi9j9zy8pr44m527i3icr5jsq02k"))))
    (build-system trivial-build-system)
    (native-inputs
     `(("p7zip" ,p7zip)))
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (setenv "PATH" (string-append
                         (assoc-ref %build-inputs "p7zip") "/bin" ":"))
         (invoke "7z" "x" (assoc-ref %build-inputs "source"))
         (let* ((out (assoc-ref %outputs "out"))
                (doc (string-append out "/doc"))
                (dir (string-append (assoc-ref %outputs "out")
                                    "/" ,duke-nukem-3d-directory)))
           (mkdir-p dir)
           (mkdir-p doc)
           (copy-recursively "addons" dir)
           (copy-recursively (string-append dir "/readme")
                             (string-append doc "/dukenukem3d"))
           (delete-file-recursively (string-append dir "/readme")))
         #t)))
    (synopsis "Selection of mods for Duke Nukem 3D")
    (description "This pack is is a compilation of great episodes and/or total
conversions for Duke Nukem 3D.")
    (home-page "http://hrp.duke4.net/")
    (license ((@@ (guix licenses) license) "Custom license"
              "http://hrp.duke4.net/misc/addons_readme.txt"
              ""))))

(define-public duke-nukem-3d-borg-nukem
  (package
    (name "duke-nukem-3d-borg-nukem")
    (version (package-version duke-nukem-3d-eduke32-addon-compilation))
    (source #f)
    (build-system trivial-build-system)
    (inputs
     `(("compilation" ,duke-nukem-3d-eduke32-addon-compilation)))
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (let ((indir (string-append (assoc-ref %build-inputs "compilation")
                                     "/" ,duke-nukem-3d-directory))
               (dir (string-append (assoc-ref %outputs "out")
                                   "/" ,duke-nukem-3d-directory)))
           (mkdir-p dir)
           (install-file (string-append indir "/borgnukem.grp") dir))
         #t)))
    (synopsis "Borg Nukem addon for Duke Nukem 3D")
    (description "Borg Nukem addon for Duke Nukem 3D") ; TODO: Extend description.
    (home-page "http://hrp.duke4.net/")
    (license ((@@ (guix licenses) license) "By Kevin 'Kef_Nukem' Cools/Borg Team"
              "No URL"
              ""))))

(define-public duke-nukem-3d-total-meltdown
  (package
    (name "duke-nukem-3d-total-meltdown")
    (version "1.0.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "http://duke64.duke4.net/downloads/duketm_v"
                           version
                           ".zip"))
       (sha256
        (base32 "07m1vw339ccxpmp9lbc9a52dnmzrbvcl7rgzlswshayiv07xrslq"))))
    (build-system trivial-build-system)
    (native-inputs
     `(("zip" ,zip)
       ("unzip" ,unzip)))
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (setenv "PATH" (string-append
                         (assoc-ref %build-inputs "zip") "/bin" ":"
                         (assoc-ref %build-inputs "unzip") "/bin" ":"))
         (invoke "unzip" (assoc-ref %build-inputs "source"))
         (let ((dir (string-append (assoc-ref %outputs "out")
                                   "/" ,duke-nukem-3d-directory)))
           (mkdir-p dir)
           (with-directory-excursion "duketm"
             (invoke "zip" "-r" (string-append dir "/duketm.zip")
                     ".")))
         #t)))
    (synopsis "Port of the PlayStation release of Duke Nukem 3D")
    (description "This total conversion intends to bring the PlayStation
experience from 1997 to your PC.")
    (home-page "http://duke64.duke4.net/")
    (license ((@@ (guix licenses) license) "No license"
              "No URL"
              ""))))

;; TODO: Add moddb mirror support.
(define-public duke-nukem-3d-64-mod
  (package
    (name "duke-nukem-3d-64-mod")
    (version "0.9.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://www.moddb.com/downloads/mirror/68412/101/9959c9f15de303acb69ad586d6808172"))
       (file-name (string-append name "-" version))
       (sha256
        (base32 "0b4dk3200mhqncp84a7i57zl2lqv10v4l5rhnwy3bv51vpll49h0"))))
    (build-system trivial-build-system)
    (native-inputs
     `(("zip" ,zip)
       ("unzip" ,unzip)))
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (setenv "PATH" (string-append
                         (assoc-ref %build-inputs "zip") "/bin" ":"
                         (assoc-ref %build-inputs "unzip") "/bin" ":"))
         (invoke "unzip" (assoc-ref %build-inputs "source"))
         (let ((dir (string-append (assoc-ref %outputs "out")
                                   "/" ,duke-nukem-3d-directory)))
           (mkdir-p dir)
           (with-directory-excursion "duke64"
             (invoke "zip" "-r" (string-append dir "/duke64.zip")
                     ".")))
         #t)))
    (synopsis "Port of the Nintendo 64 release of Duke Nukem 3D")
    (description "This total conversion intends to bring the Nintendo experience
from 1997 to your PC.")
    (home-page "http://duke64.duke4.net/")
    (license ((@@ (guix licenses) license) "No license"
              "No URL"
              ""))))
